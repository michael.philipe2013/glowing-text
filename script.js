const Button = document.getElementById("button");
const ButtonGoBack = document.getElementById("goBack");
const LIST_LETTERS = document.getElementById("list-letters");
const MODAL = document.getElementById("modal");
const NAME = document.getElementById("getName");

const getName = () => {
  const Name = NAME.value.split("");
  MODAL.style.display = "flex";
  Name.map((letter) => {
    const CAPITALIZED_LETTER = letter.toUpperCase();
    const LI = document.createElement("li");
    const INPUT = document.createElement("input");
    INPUT.type = "checkbox";
    const DIV = document.createElement("div");
    DIV.className = "div-letters";
    DIV.innerText = CAPITALIZED_LETTER;
    LI.appendChild(INPUT);
    LI.appendChild(DIV);
    LIST_LETTERS.appendChild(LI);
  });
};

const goBack = () => {
  MODAL.style.display = "none";
  LIST_LETTERS.innerHTML = "";
  NAME.value = "";
};

ButtonGoBack.addEventListener("click", goBack);
Button.addEventListener("click", getName);
